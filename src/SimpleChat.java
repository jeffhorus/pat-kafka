import kafka.admin.AdminUtils;
import kafka.consumer.Consumer;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;
import kafka.utils.ZKStringSerializer$;
import org.I0Itec.zkclient.ZkClient;

import java.util.*;

/**
 * Created by jelink on 15/10/15.
 */
public class SimpleChat extends Thread {

    // Milik Producer Saja
    protected kafka.javaapi.producer.Producer<String,String> producer;
    protected String username;
    protected HashMap<String, SimpleChat> topicsJoined;

    // Milik Consumer Saja
    protected String topic;
    protected ConsumerConnector consumerConnector;

    // Producer
    public SimpleChat() {
        topicsJoined = new HashMap<>();
        // init user with random number
        username = Double.toString(Math.random());

        // init producing commands
        setupProducer();
        startProducing();
    }

    // Consumer
    public SimpleChat(String username, String topicName) {
        this.username = username;
        topic = topicName;
    }

    public static void main(String[] argv) {
        SimpleChat client = new SimpleChat();
        //client.start();
    }

    public void startConsumer() {
        Properties properties = new Properties();
        properties.put("zookeeper.connect","localhost:2181");
        properties.put("group.id",username + "-group");
        properties.put("auto.offset.reset", "smallest");
        ConsumerConfig consumerConfig = new ConsumerConfig(properties);
        consumerConnector = Consumer.createJavaConsumerConnector(consumerConfig);
    }

    public void setupProducer() {
        Properties properties = new Properties();
        properties.put("metadata.broker.list","localhost:9092");
        properties.put("serializer.class","kafka.serializer.StringEncoder");
        ProducerConfig producerConfig = new ProducerConfig(properties);
        producer = new kafka.javaapi.producer.Producer<String, String>(producerConfig);
    }

    public void startProducing() {
        Scanner sc = new Scanner(System.in);
        String command = sc.nextLine();
        String temp = command.toLowerCase();
        while (!temp.startsWith("/exit")) {
            if (temp.startsWith("/join")) {
                String[] commands = temp.split(" ");
                try {
                    String topicName = commands[1];
                    if (topicsJoined.containsKey(topicName)) {
                        System.out.println("Already a member of channel " + topicName);
                    } else {
                        SimpleChat currentChannel = new SimpleChat(username, topicName);
                        topicsJoined.put(topicName, currentChannel);
                        currentChannel.start();
                        System.out.println("Successfully joined to " + topicName);
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Please specify channel name to join");
                }
            } else if (temp.startsWith("/leave")) {
                try {
                    String[] commands = temp.split(" ");
                    String topicName = commands[1];
                    if (topicsJoined.containsKey(topicName)) {
                        SimpleChat leavedTopic = topicsJoined.get(topicName);
                        leavedTopic.shutDownConsumer();
                        topicsJoined.remove(topicName);
                        System.out.println("Leaving channel " + topicName);
                    } else {
                        System.out.println("Not a member of " + topicName);
                    }
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Please specify channel name to leave");
                    if (topicsJoined.size() > 0) {
                        System.out.println("Your channels : ");
                        Iterator it = topicsJoined.entrySet().iterator();
                        while(it.hasNext()) {
                            Map.Entry pair = (Map.Entry) it.next();
                            String topicName = (String) pair.getKey();
                            System.out.println(topicName);
                        }
                    }
                }
            } else if (temp.startsWith("/nick")) {
                String[] commands = temp.split(" ");
                try {
                    username = commands[1];
                    System.out.println("Successfully changed name to " + username);
                } catch (ArrayIndexOutOfBoundsException e) {
                    System.out.println("Please specify a nickname");
                    System.out.println("Your current nickname is " + username);
                }
            } else if (temp.startsWith("@")) {
                String[] commands = temp.split(" ");
                String targetTopic = commands[0].substring(1);
                String text = temp.substring(targetTopic.length()+2);
                sendMessageTo(targetTopic, text);
            }
            else {
                broadcastTestMessage(command);
            }
            command = sc.nextLine();
            temp = command.toLowerCase();
        }
        exitChat();
        producer.close();
        System.out.println("exiting program");
    }

    private void exitChat() {
        Iterator it = topicsJoined.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            SimpleChat topic = (SimpleChat) pair.getValue();
            topic.shutDownConsumer();
        }
    }

    public void shutDownConsumer() {
        consumerConnector.shutdown();
    }

    private void broadcastTestMessage(String text) {
        // tambahkan username ke text
        text = "(" + username + ") " + text;
        Iterator it = topicsJoined.entrySet().iterator();
        while(it.hasNext()) {
            Map.Entry pair = (Map.Entry) it.next();
            String topicName = (String) pair.getKey();
            sendMessageTo(topicName, text);
        }
    }

    private void sendMessageTo(String topicName, String text) {
        KeyedMessage<String, String> message = new KeyedMessage<String, String>(topicName, text);
        producer.send(message);
    }

    @Override
    public void start() {
        // urusan consumer
        startConsumer();
        super.start();

        // urusan producer
//        setupProducer();
//        startProducing();
    }

    @Override
    public void run() {
        Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(topic, new Integer(1));
        Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumerConnector.createMessageStreams(topicCountMap);
        KafkaStream<byte[], byte[]> stream =  consumerMap.get(topic).get(0);
        ConsumerIterator<byte[], byte[]> it = stream.iterator();

        // cetak semua pesan
        while(!this.isInterrupted() && it.hasNext()) {
            System.out.println("[" + topic + "] " + new String(it.next().message()));
        }
    }
}
