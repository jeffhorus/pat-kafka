# IF4031-KAFKA
Implementation of IRC-Like Chatting System using KAFKA in Ubuntu using Java

By:
- Christ Angga Saputra - 13512019
- Jeffrey Lingga Binangkit - 13512059

## Petunjuk instalasi/building
1. Ada 2 cara untuk instalasi program ini, yaitu:
  - Clone dari `https://bitbucket.org/jeffhorus/pat-kafka`
  - Ekstrak source code

2. Selanjutnya `Import Project` dengan menggunakan IntelliJ Idea

## Cara menjalankan program
1. Jalankan server zookeeper adalah dengan cara membuka folder `/src/kafka_2.9.1-0.8.2.2/` kemudian jalankan script `bin/zookeeper-server-start.sh config/zookeeper.properties`
2. Jalankan server KAFKA dengan menjalankan script `bin/kafka-server-start.sh config/server.properties` di folder tadi
3. Setelah dua script tada sudah dijalankan baru dapat dilakukan jalankan `SimpleChat.java` dengan klik tombol Run terhadap file tersebut

## Daftar tes yang telah dilakukan serta langkah2 melakukan tes

### Mengganti nickname
1. PERSIAPAN
  - Jalankan server zookeeper dan kafka
  - Jalankan `SimpleChat.java` sebagai aplikasi client

2. PENGETESAN
  - Masukkan perintah `/NICK lingga`

3. HASIL
  - Pada client muncul `Successfully changed name to lingga`

### Memasukkan perintah nick namun tidak menspesifikasikan nicknamenya
1. PERSIAPAN
  - Jalankan server zookeeper dan kafka
  - Jalankan satu client

2. PENGETESAN
  - Pada client, masukkan perintah `/NICK`

3. HASIL
  - Pada client dua muncul `Please specify a nickname. Your current nickname is [username]`

### Melakukan join ke channel pat
1. PERSIAPAN
  - Jalankan server zookeeper dan kafka
  - Jalankan satu client

2. PENGETESAN
  - Masukkan perintah `/JOIN pat`

3. HASIL
  - Pada client muncul `Successfully joined to pat`

### Melakukan join ke channel pat yang sudah di-join sebelumnya
1. PERSIAPAN
  - Jalankan server
  - Jalankan satu client
  - Jalankan `/JOIN pat`

2. PENGETESAN
  - Masukkan kembali perintah `/JOIN pat`

3. HASIL
  - Pada client muncul `Already a member of channel pat`

### Pengetesan leave channel yang diikuti
1. PERSIAPAN
  - Jalankan server
  - Jalankan satu client dengan nick lingga
  - Jalankan perintah `/JOIN pat`

2. PENGETESAN
  - Jalankan perintah `/LEAVE pat`

3. HASIL
  - Pada client muncul `Leaving channel pat`

### Pengetesan leave channel yang tidak diikuti
1. PERSIAPAN
  - Jalankan server
  - Jalankan dua client dengan nick christ dan lingga
  - Lakukan join ke pat oleh client christ

2. PENGETESAN
  - Pada client lingga, jalankan perintah `/LEAVE pat`

3. HASIL
  - Pada client muncul `Not a member of pat`

### Pengetesan exit aplikasi
1. PERSIAPAN
  - Jalankan server
  - Jalankan satu client dengan nick lingga

2. PENGETESAN
  - Jalankan perintah `/EXIT`

3. HASIL
  - Pada client muncul `exiting program` kemudian keluar dari program chat

### Pengetesan chatting secara kompleks dimana terdapat dua channel channel_a dan channel_b, dan ada empat client ab, a, b, dan c
1. PERSIAPAN
  - Jalankan server
  - Jalankan empat client dengan nick ab, a, b, dan c
  - Lakukan join ab ke channel_a dan channel_b
  - Lakukan join a ke channel_a
  - Lakukan join b ke channel_b
  - Client c tidak join

2. PENGETESAN
  - Pada client ab, masukkan `hai channel A dan B`

3. HASIL
  - Pada client ab, muncul `[channel_a] (ab) hai channel A dan B` dan `[channel_b] (ab) hai channel A dan B`
  - Pada client a, muncul `[channel_a] (ab) hai channel A dan B`
  - Pada client b, muncul `[channel_b] (ab) hai channel A dan B`
  - Pada client c, tidak muncul apa-apa

### Melakukan chatting antara beberapa client dalam satu channel PAT
1. PERSIAPAN
  - Jalankan server
  - Jalankan tiga client dengan nick a, b, dan c
  - Lakukan join ke channel pat oleh ketiga client tersebut

2. PENGETESAN
  - Pada client a, masukkan `@pat hai b dan c`

3. HASIL
  - Pada client a, muncul `[pat] (a) hai b dan c`
  - Pada client b, muncul `[pat] (a) hai b dan c`
  - Pada client c, muncul `[pat] (a) hai b dan c`